//
// Created by Vasyl Ahosta on 6/15/17.
// Copyright (c) 2017 Vasyl Ahosta. All rights reserved.
//

import Foundation

public protocol EventSubscriptionToken: class {

    func unsubscribe()

}

public class Event<T> {

    public typealias Handler = (T) -> ()

    internal weak var delegate: EventDelegate?
    internal var handlers: [HandlerWrapper] = []

    public init() {}

    public func subscribe(_ handler: @escaping Handler) -> EventSubscriptionToken {
        return subscribe(skipFirst: false, handler)
    }

    public func subscribe(skipFirst: Bool, _ handler: @escaping Handler) -> EventSubscriptionToken {
        let wrapper = HandlerWrapper(handler, skipFirst)
        handlers.append(wrapper);
        delegate?.event(self, didAddHandler: wrapper)
        return SubscriptionToken(event: self, handlerWrapper: wrapper)
    }

    public func raise(_ userInfo: T) {
        handlers.forEach { $0.raise(userInfo) }
    }

    public func bind<Target: AnyObject>(_ keyPath: WritableKeyPath<Target, T>, of target: Target, skipFirst: Bool = false) -> EventSubscriptionToken {
        return subscribe(skipFirst: skipFirst) { [weak target] userInfo in
            target?[keyPath: keyPath] = userInfo
        }
    }

    public func bind<Target: AnyObject, V>(_ keyPath: WritableKeyPath<Target, V>, of target: Target, skipFirst: Bool = false, map: @escaping (T) -> V) -> EventSubscriptionToken {
        return subscribe(skipFirst: skipFirst) { [weak target] userInfo in
            target?[keyPath: keyPath] = map(userInfo)
        }
    }

    public func bind(to event: Event<T>, skipFirst: Bool = false) -> EventSubscriptionToken {
        return event.subscribe(skipFirst: skipFirst) { [weak self] userInfo in
            self?.raise(userInfo)
        }
    }

    internal func remove(_ handlerWrapper: HandlerWrapper) {
        if let index = handlers.firstIndex(where: { $0 === handlerWrapper }) {
            handlers.remove(at: index)
        }
    }

    public func subscribeForOnce(skipFirst: Bool = false, _ handler: @escaping Handler) {
        var token: EventSubscriptionToken?
        token = subscribe(skipFirst: skipFirst) { userInfo in
            handler(userInfo)
            if token != nil { token = nil }
        }
    }

}

public func +=<T>(event: Event<T>, handler: @escaping Event<T>.Handler) -> EventSubscriptionToken {
    return event.subscribe(handler)
}

extension Event {

    internal final class HandlerWrapper {

        private let handler: Handler
        private var skipFirst: Bool

        init(_ handler: @escaping Handler, _ skipFirst: Bool) {
            self.handler = handler
            self.skipFirst = skipFirst
        }

        func raise(_ userInfo: T) {
            if !skipFirst {
                handler(userInfo)
            } else {
                skipFirst = false
            }
        }

    }

}

extension Event {

    internal final class SubscriptionToken: EventSubscriptionToken {

        weak var handlerWrapper: HandlerWrapper?
        weak var event: Event<T>?

        init(event: Event<T>, handlerWrapper: HandlerWrapper) {
            self.event = event
            self.handlerWrapper = handlerWrapper
        }

        deinit {
            unsubscribe()
        }

        public func unsubscribe() {
            if let handlerWrapper = handlerWrapper {
                event?.remove(handlerWrapper)
            }
        }

    }

}

internal protocol EventDelegate: class {

    func event<T>(_ event: Event<T>, didAddHandler handlerWrapper: Event<T>.HandlerWrapper)

}

extension Event where T == Void {

    public func raise() {
        return raise(())
    }

}
