//
//  Created by Vasyl Ahosta on 5/9/18.
//  Copyright © 2018 Vasyl Ahosta. All rights reserved.
//

import XCTest
@testable import SwiftEvents

final class EventSubscriberTests: XCTestCase {

    var eventSubscriptionTokens: [EventSubscriptionToken] = []

    func testSubscribe_ShouldAddTokenToEventSubscriptionTokens() {
        let event = Event<Void>()
        subscribe(for: event) { _ in }
        assertEventSubscriptionTokensHasOneItem()
    }

    func testSubscribe_ShouldAddHandler() {
        let event = Event<Void>()
        subscribe(for: event) { _ in }
        XCTAssertTrue(event.hasHandler(with: eventSubscriptionTokens[0]))
    }

    func testUnsubscribeFromAllEvents_ShouldRemoveAllTokens() {
        eventSubscriptionTokens.append(EventSubscriptionTokenDummy())
        unsubscribeFromAllEvents()
        XCTAssertTrue(eventSubscriptionTokens.isEmpty)
    }

    func testBindObjectToEvent_ShouldAddTokenToToEventSubscriptionTokens() {
        bind(TestObject(), \.stringValue, to: Event<String>())
        assertEventSubscriptionTokensHasOneItem()
    }

    func testBindObjectToEvent_ShouldAddHandler() {
        let event = Event<String>()
        bind(TestObject(), \.stringValue, to: event)
        XCTAssertTrue(event.hasHandler(with: eventSubscriptionTokens[0]))
    }

    func testBindObjectToEventWithMap_ShouldAddTokenToToEventSubscriptionTokens() {
        bind(TestObject(), \.stringValue, to: Event<Int>(), map: { _ in "" })
        assertEventSubscriptionTokensHasOneItem()
    }

    func testBindObjectToEventWithMap_ShouldAddHandler() {
        let event = Event<Int>()
        bind(TestObject(), \.stringValue, to: event, map: { _ in "" })
        XCTAssertTrue(event.hasHandler(with: eventSubscriptionTokens[0]))
    }

    func testBindObjectToEventWithChange_ShouldAddTokenToToEventSubscriptionTokens() {
        bind(TestObject(), \.stringValue, to: Event<Observable<String>.Change<String>>())
        assertEventSubscriptionTokensHasOneItem()
    }

    func testBindObjectToEventWithChange_ShouldAddHandler() {
        let event = Event<Observable<String>.Change<String>>()
        bind(TestObject(), \.stringValue, to: event)
        XCTAssertTrue(event.hasHandler(with: eventSubscriptionTokens[0]))
    }

    func testBindEventToEvent_ShouldAddTokenToToEventSubscriptionTokens() {
        bind(Event<Int>(), to: Event<Int>())
        assertEventSubscriptionTokensHasOneItem()
    }

    func testBindObservableToEvent_ShouldAddTokenToToEventSubscriptionTokens() {
        bind(Observable(value: ""), to: Event<String>())
        assertEventSubscriptionTokensHasOneItem()
    }

    func testBindObservableToEventWithChange_ShouldAddTokenToToEventSubscriptionTokens() {
        bind(Observable(value: ""), to: Event<Observable<String>.Change<String>>())
        assertEventSubscriptionTokensHasOneItem()
    }

    func testWhenBoundObservableWithEquatableValueToEventThenWillChangeDidChangeEventShouldWork() {
        let observable = Observable(value: 0)
        let event = Event<Int>()
        bind(observable, to: event)
        assertRaisedChangeEvents(of: observable) { event.raise(1) }
    }

    func testBindObservableToEventWithMap_ShouldAddTokenToToEventSubscriptionTokens() {
        bind(Observable(value: ""), to: Event<Int>(), map: { String(describing: $0) })
        assertEventSubscriptionTokensHasOneItem()
    }

    func testBindObservableToEventWithMap_ShouldRaiseWillChangeDidChangeEvents() {
        let observable = Observable(value: "")
        let event = Event<Int>()
        bind(observable, to: event, map: { String(describing: $0) })
        assertRaisedChangeEvents(of: observable) { event.raise(1) }
    }

    func testBindObservableToEventWithChange_ShouldRaiseEvents() {
        let observable = Observable(value: 0)
        let event = Event<Observable<Int>.Change<Int>>()
        bind(observable, to: event)
        assertRaisedChangeEvents(of: observable) { event.raise(Observable.Change(oldValue: 0, newValue: 1)) }
    }

    func testBindObservableToEventWithChangeAndWithMap_ShouldRaiseEvents() {
        let observable = Observable(value: "")
        let event = Event<Observable<Int>.Change<Int>>()
        bind(observable, to: event, map: { "\($0.newValue)"  })
        assertRaisedChangeEvents(of: observable) { event.raise(Observable.Change(oldValue: 0, newValue: 1)) }
    }

    private func assertRaisedChangeEvents<T: Equatable>(of observable: Observable<T>, on action: () -> ()) {
        var eventLog = ""
        eventSubscriptionTokens.append(observable.willChangeValueEvent.subscribe(skipFirst: true, { _ in
            eventLog += "->willChange" 
        }))
        eventSubscriptionTokens.append(observable.didChangeValueEvent.subscribe(skipFirst: true, { _ in
            eventLog += "->didChange" 
        }))
        action()
        XCTAssertEqual("->willChange->didChange", eventLog)
    }

    private func assertEventSubscriptionTokensHasOneItem() {
        XCTAssertEqual(1, eventSubscriptionTokens.count)
    }

}

extension EventSubscriberTests: EventSubscriber {
}

final class EventSubscriptionTokenDummy: EventSubscriptionToken {

    func unsubscribe() {}

}
