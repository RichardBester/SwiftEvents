//
// Created by Vasyl Ahosta on 2019-01-06.
// Copyright (c) 2019 Vasyl Ahosta. All rights reserved.
//

import XCTest
import SwiftEvents

final class EventSubscriptionTokenBagTests: XCTestCase {

    func testAdd_ShouldStoreTokenInBag() {
        let bag = makeBag()
        var token: EventSubscriptionToken? = makeToken()
        bag.add(token!)
        assertStored(&token)
    }

    func testAdd_ShouldStoreMultipleTokensInBag() {
        let bag = makeBag()
        var token0: EventSubscriptionToken? = makeToken()
        var token1: EventSubscriptionToken? = makeToken()
        bag.add(token0!)
        bag.add(token1!)
        assertStored(&token0)
        assertStored(&token1)
    }

    private func assertStored(_ token: inout EventSubscriptionToken?) {
        weak var weakToken = token
        token = nil
        XCTAssertNotNil(weakToken, "Did not store token")
    }

    func testClear_ShouldRemoveStoredToken() {
        var (bag, token) = makeBagWithAToken()
        bag.clear()
        assertRemoved(&token)
    }

    private func assertRemoved(_ token: inout EventSubscriptionToken?) {
        weak var weakToken = token
        token = nil
        XCTAssertNil(weakToken, "Did not remove token")
    }

}

extension EventSubscriptionTokenBagTests {

    private func makeBagWithAToken() -> (EventSubscriptionTokenBag, EventSubscriptionToken?) {
        let bag = makeBag()
        let token: EventSubscriptionToken? = makeToken()
        bag.add(token!)
        return (bag, token)
    }

    private func makeToken() -> EventSubscriptionToken {
        return Event<Void>().subscribe({ _ in })
    }

    private func makeBag() -> EventSubscriptionTokenBag {
        return EventSubscriptionTokenBag()
    }

}
