//
// Created by Vasyl Ahosta on 6/13/17.
// Copyright (c) 2017 Vasyl Ahosta. All rights reserved.
//

import XCTest
@testable import SwiftEvents

final class EventTests: TestCase {

    private var token: EventSubscriptionToken?
    private var bindingToken: EventSubscriptionToken?

    func testAddHandler_ShouldAddMultipleHandlers() {
        let event = Event<String>()
        var tokens = [EventSubscriptionToken]()
        for _ in 0...6 {
            tokens.append(event.subscribe({ arg in }))
        }
        tokens.forEach { XCTAssertTrue(event.hasHandler(with: $0)) }
    }

    func testAddHandlerOperator_ShouldAddHandler() {
        let event = Event<String>()
        let token = event += { arg in }
        XCTAssertTrue(event.hasHandler(with: token))
    }

    func testRaise_ShouldIvokeAllHandlers() {
        let event = Event<String>()
        let handlerCount = 6
        var invocationCount = 0
        var tokens: [EventSubscriptionToken] = []
        for _ in 0..<handlerCount {
            let token = event += { _ in invocationCount += 1 }
            tokens.append(token)
        }
        event.raise("user info")
        XCTAssertEqual(handlerCount, invocationCount)
    }

    func testRaise_ShouldPassUserInfoToHandler() {
        let event = Event<String>()
        let userInfo = "user info"
        _ = event += { XCTAssertEqual(userInfo, $0)}
        event.raise(userInfo)
    }

    func testRemoveHandlerOperator_ShouldRemoveHandlerByToken() {
        let event = Event<Void>()
        let token1 = event += { }
        let token2 = event += { }
        token1.unsubscribe()
        XCTAssertFalse(event.hasHandler(with: token1))
        XCTAssertTrue(event.hasHandler(with: token2))
    }

    func testEventHandlerTokenRemove_ShouldRemoveHandlerFromEvent() {
        let event = Event<Int>()
        let token = event += { _ in }
        XCTAssertTrue(event.hasHandler(with: token))
        token.unsubscribe()
        XCTAssertFalse(event.hasHandler(with: token))
    }

    func testEventAndTokenShouldNotRetainEachOther() {
        weak var event: Event<Int>?
        weak var token: EventSubscriptionToken?
        do {
            let anEvent = Event<Int>()
            let aToken = anEvent += { _ in }
            event = anEvent
            token = aToken
            XCTAssertNotNil(event)
            XCTAssertNotNil(token)
        }
        XCTAssertNil(event)
        XCTAssertNil(token)
    }

    func testEventOfVoidShouldRaiseWithoutTupleArgument() {
        let event: Event<Void> = Event()
        event.raise()
    }

    func testWhenEventHandlerTokenGetsDeinitializedThenHandlerShouldBeRemoved() {
        let event = Event<Void>()
        token = event += { _ in }
        token = nil
        XCTAssertEqual(0, event.handlers.count)
    }

    func testWhenEventRaisesThenBoundObjectPropertyShouldBeSetUserInfoFromTheEvent() {
        let someValue = "String Value"
        let event = Event<String>()
        let object = TestObject()
        token = event.bind(\.stringValue, of: object)
        event.raise(someValue)
        XCTAssertEqual(someValue, object.stringValue)
    }

    func testBindWithMap_ShouldTransformEventsUserInfoToObjectsProperty() {
        let value = 2
        let event = Event<Int>()
        let object = TestObject()
        token = event.bind(\.stringValue, of: object, map: { "\($0)" })
        event.raise(value)
        XCTAssertEqual("\(value)", object.stringValue)
    }

    func testBindEventToEvent() {
        let targetEvent = Event<Int>()
        let sourceEvent = Event<Int>()
        bindingToken = targetEvent.bind(to: sourceEvent)
        let expectedUserInfo = 1
        var actualUserInfo = 0
        token = targetEvent += { actualUserInfo = $0 }
        sourceEvent.raise(expectedUserInfo)
        XCTAssertEqual(expectedUserInfo, actualUserInfo)
    }

    func testBoundEventsShouldNotRetainEachOther() {
        weak var targetEvent: Event<Int>?
        weak var sourceEvent: Event<Int>?
        do {
            let theTargetEvent = Event<Int>()
            let theSourceEvent = Event<Int>()
            targetEvent = theTargetEvent
            sourceEvent = theSourceEvent
            bindingToken = theTargetEvent.bind(to: theSourceEvent)
        }
        XCTAssertNil(targetEvent)
        XCTAssertNil(sourceEvent)
    }

    func testSubscribeForOnce_ShouldAddHandlerThatIsToBeRemovedOnFirsRaise() {
        let event = Event<String>()
        var raiseCounter: Int = 0
        let expectedUserInfo = "qwerty"
        event.subscribeForOnce { userInfo in
            raiseCounter += 1
            XCTAssertEqual(expectedUserInfo, userInfo)
        }
        event.raise(expectedUserInfo)
        XCTAssertEqual(1, raiseCounter)
        event.raise(expectedUserInfo)
        XCTAssertEqual(1, raiseCounter)
    }

    func testSubscribeForOnce_WhenEventDiesSoShouldDoTheHandler() {
        var event: Event<String>? = Event()
        event?.subscribeForOnce { _ in }
        weak var weakHandler = event?.handlers[0]
        event = nil
        XCTAssertNil(weakHandler)
    }

    func testSubscribeSkippingFirst_WhenRaisedForFirstTimeThenShouldNotCallHandler() {
        let event = Event<String>()
        var raiseCounter: Int = 0
        token = event.subscribe(skipFirst: true) { _ in
            raiseCounter += 1
        }
        event.raise("")
        XCTAssertEqual(0, raiseCounter)
        event.raise("")
        XCTAssertEqual(1, raiseCounter)
    }

    func testSubscribeForOnceSkippingFirst_WhenRaisedForFirstTimeThenShouldNotCallHandler() {
        let event = Event<String>()
        var raiseCounter: Int = 0
        event.subscribeForOnce(skipFirst: true) { _ in
            raiseCounter += 1
        }
        event.raise("")
        XCTAssertEqual(0, raiseCounter)
        event.raise("")
        XCTAssertEqual(1, raiseCounter)
        event.raise("")
        XCTAssertEqual(1, raiseCounter)
    }

    func testBindToEventSkippingFirst_WhenSourceEventRaisedForFirstTimeThenDoRiseTargetEvent() {
        let targetEvent = Event<Int>()
        let sourceEvent = Event<Int>()
        bindingToken = targetEvent.bind(to: sourceEvent, skipFirst: true)
        assertDidNotRaise(targetEvent, on: sourceEvent.raise(0))
        assertRaised(targetEvent, on: sourceEvent.raise(0))
    }

    func testBindObjectSkippingFirst_WhenRaisedForFirstTimeThenDoNotSetBoundProperty() {
        let event = Event<String>()
        let object = TestObject()
        token = event.bind(\.stringValue, of: object, skipFirst: true)
        event.raise("Q")
        XCTAssertTrue(object.stringValue.isEmpty, "Shouldn't set new value")
    }

    func testBindObjectWithMappingAndSkippingFirst_WhenRaisedForFirstTimeThenDoNotSetBoundProperty() {
        let event = Event<Int>()
        let object = TestObject()
        token = event.bind(\.stringValue, of: object, skipFirst: true, map: { "\($0)" })
        event.raise(1)
        XCTAssertTrue(object.stringValue.isEmpty, "Shouldn't set new value")
    }

}

extension Event {

    func hasHandler(with token: EventSubscriptionToken) -> Bool {
        return handlers.first(where: { $0 === (token as! SubscriptionToken).handlerWrapper }) != nil
    }

}
