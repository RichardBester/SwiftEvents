//
// Created by Vasyl Ahosta on 9/11/19.
// Copyright (c) 2019 Vasyl Ahosta. All rights reserved.
//

import XCTest
@testable import SwiftEvents

final class ObservableWithEquatableValueTests: TestCase {

    private var token: EventSubscriptionToken?

    func testWhenBoundToEventSkippingFirstThenShouldNotSetValueForFirstTime() {
        let event = Event<String>()
        let observable = Observable(value: "")
        token = observable.bind(to: event, skipFirst: true)
        let newValue = "NewValue"
        event.raise(newValue)
        XCTAssertTrue(observable.value.isEmpty, "Should not set value")
        assertRaised(observable.didChangeValueEvent, skipFirst: true, on: event.raise(newValue), message: "Should use equatable binding")
        XCTAssertEqual(newValue, observable.value)
    }

    func testWhenBoundToEventWithMapSkippingFirstThenShouldNotSetValueForFirstTime() {
        let event = Event<Int>()
        let observable = Observable(value: "")
        token = observable.bind(to: event, skipFirst: true) { "\($0)" }
        event.raise(1)
        XCTAssertTrue(observable.value.isEmpty, "Should not set value")
        assertRaised(observable.didChangeValueEvent, skipFirst: true, on: event.raise(1), message: "Should use equatable binding")
        XCTAssertEqual("1", observable.value)
    }

    func testWhenBoundToEventWithChangeSkippingFirstThenShouldNotSetValueForFirstTime() {
        let event = Event<Observable<String>.Change<String>>()
        let observable = Observable(value: "")
        token = observable.bind(to: event, skipFirst: true)
        let newValue = Observable<String>.Change<String>(oldValue: nil, newValue: "NewValue")
        event.raise(newValue)
        XCTAssertTrue(observable.value.isEmpty, "Should not set value")
        assertRaised(observable.didChangeValueEvent, skipFirst: true, on: event.raise(newValue), message: "Should use equatable binding")
        XCTAssertEqual(newValue.newValue, observable.value)
    }

    func testGivenBoundObjectPropertyWhenChangedValueThenSetNewValueToTheProperty() {
        let object = TestObject()
        let observable = Observable(value: "")
        token = observable.didChangeValueEvent.bind(\.stringValue, of: object)
        let newValue = "New Value"
        observable =~ newValue
        XCTAssertEqual(newValue, object.stringValue)
    }

    func testWhenBoundObjectPropertySkippingFirstThenDoNotSetNewValueToTheProperty() {
        let object = TestObject()
        let observable = Observable(value: "initial value")
        token = observable.didChangeValueEvent.bind(\.stringValue, of: object, skipFirst: true)
        XCTAssertTrue(object.stringValue.isEmpty)
    }

}
