//
// Created by Vasyl Ahosta on 9/10/19.
// Copyright (c) 2019 Vasyl Ahosta. All rights reserved.
//

import XCTest
@testable import SwiftEvents

class TestCase: XCTestCase {

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
    }

    func assertRaised<T>(_ event: Event<T>, skipFirst: Bool = false, on action: @autoclosure () -> Void, callback: ((T) -> Void)? = nil, message: String = "") {
        var eventLog: String = ""
        let token = event.subscribe(skipFirst: skipFirst) { value in
            eventLog += "->raised"
            callback?(value)
        }
        action()
        XCTAssertEqual("->raised", eventLog, message)
        token.unsubscribe()
    }

    func assertDidNotRaise<T>(_ event: Event<T>, on action: @autoclosure () -> Void) {
        var eventLog: String = ""
        let token = event += { _ in eventLog += "->raised"}
        action()
        XCTAssertTrue(eventLog.isEmpty, "Raised event")
        token.unsubscribe()
    }

}
